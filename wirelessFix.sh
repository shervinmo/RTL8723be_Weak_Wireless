#!/bin/bash
sudo apt-get install linux-headers-generic build-essential git net-tools
var1=$(ifconfig -s | sed -n '4p'| cut -d " " -f 1)
if [ ! -d "$HOME/rtlwifi_new/"];then
    cd $HOME
	git clone https://github.com/lwfinger/rtlwifi_new.git $HOME/rtlwifi_new
fi
cd $HOME/rtlwifi_new/
make clean
git pull
make 
sudo make install
sudo modprobe -rv rtl8723be
sudo modprobe -v rtl8723be ant_sel=2
sudo ip link set $var1 up
sudo iw dev $var1 scan
echo "options rtl8723be ant_sel=2" | sudo tee /etc/modprobe.d/50-rtl8723be.conf
